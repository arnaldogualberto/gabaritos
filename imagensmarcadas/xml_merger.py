import numpy as np
import pandas as pd
import sys
from lxml import etree as et

def nms(rects, overlap_t):
    if len(rects) == 0:
        return []
    
    pick = []
    
    x1 = np.array([tl[0] for tl, _ in rects])
    y1 = np.array([tl[1] for tl, _ in rects])
    x2 = np.array([br[0] for _, br in rects])
    y2 = np.array([br[1] for _, br in rects])
    
    area = (x2 - x1 + 1)*(y2 - y1 + 1)
    idxs = np.argsort(y2)
    
    while len(idxs) > 0:
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        overlap = (w * h)/area[idxs[:last]]
        
        idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlap_t)[0])))
        
    return [rects[i] for i in pick]

def xml2df(xml_path):
    columns = ['file', 'top', 'left', 'width', 'height']
    df = pd.DataFrame(columns=columns)
    
    parser = et.XMLParser(remove_blank_text=True)
    file = et.parse(xml_path, parser=parser)
    dataset = file.getroot()
    images = list(dataset.getchildren()[2])
    n_images = len(images)
    for i, image in enumerate(images):
        print('\r{0:=5} of {1} images'.format(i+1, n_images), end='')
        file = image.get(columns[0])
        rects = []
        for box in image:
            info = [int(box.get(att)) for att in columns[1:]]
            rects.append([file]+info)
        df = df.append(pd.DataFrame(rects, columns=columns), ignore_index=True)
    return df

def df2dict(df):
    files = df.file.unique()
    n_files = len(files)

    dict_rects = {}
    for i, file in enumerate(files):
        print('\r{0:=5} of {1} images'.format(i+1, n_files), end='')

        rects = []
        for _, top, left, width, height in df[df.file == file].values:
            rects.append(((left, top), (left+width, top+height)))
        dict_rects[file] = nms(rects, 0.3)
    return dict_rects

def dict2xml(dict_rects):
    from lxml import etree as et
    from os.path import isfile
    from os import remove
    
    xml_path = 'output.xml'
    if isfile(xml_path): remove(xml_path)

    dataset = et.Element('dataset')
    name = et.SubElement(dataset, 'name')
    name.text = "Vsoft Dataset Annotator"
    et.SubElement(dataset, 'comment', {'folderPath':''})
    images = et.SubElement(dataset, 'images')

    for filename, rects in dict_rects.items():
        image = et.SubElement(images, 'image', {'file':filename})
        
        for tl, br in rects:
            et.SubElement(image, 'box', {'top':str(tl[1]), 'left':str(tl[0]), 'width':str(br[0]-tl[0]+1), 'height':str(br[1]-tl[1]+1)})
        
    file = et.ElementTree(dataset)
    file.write(xml_path, encoding='iso-8859-1', xml_declaration=True, pretty_print=True)

if len(sys.argv) != 3:
    print("You must pass two xml files to merge")
    sys.exit(0)

args = sys.argv
path_1, path_2 = args[-2], args[-1]
print("xml_1: {} \nxml_2: {}".format(path_1, path_2), end='\n\n')

print("Loading first xml...")
df_1 = xml2df(path_1)
print("\nThe first xml has {} boxes".format(df_1.shape[0]), end='\n\n')

print("Loading second xml...")
df_2 = xml2df(path_2)
print("\nThe second xml has {} boxes".format(df_2.shape[0]), end='\n\n')

df_out = df_1.append(df_2, ignore_index=True)
print("The output xml has {} boxes in {} images before nms".format(df_out.shape[0], len(df_out.file.unique())))
print("Applying nms to remove duplicated boxes...")
dict_res = df2dict(df_out)

len_rects = [len(dict_res[key]) for key in dict_res]
print("\nThe output xml has {} boxes in {} images after nms".format(sum(len_rects), len(len_rects)))

print("\nSaving output.xml ...")
dict2xml(dict_res)
print("Done.")